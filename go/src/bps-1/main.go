package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math"
	"net"
	"os"
	"strconv"
)

// const FREEFALL_ACCELERATION 9.807 // on earth on equator sea level? ie. gravity?
const FREEFALL_ACCELERATION = 1 // This is of no use before further insight on the engine is gained.

var friction float32 = 0.5 // usually 0–1.0, though nothing prevents values > 1.0.
var radius, speed float32
var last_position, current_position float32

// max speed for a curve of a given radius.
func getMaxSpeed(radius, friction float32) float32 {
	return float32(math.Sqrt(float64(friction * radius * FREEFALL_ACCELERATION)))
}

// simple max throttle function.

func getMaxThrottle(radius, friction float32) float32 {
	if speed > getMaxSpeed(radius, friction) {
		return 0.0
	} else {
		return 1.0
	}
}

func connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

func read_msg(reader *bufio.Reader) (msg interface{}, err error) {
	var line string
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(line), &msg)
	if err != nil {
		return
	}
	return
}

func write_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func send_join(writer *bufio.Writer, name string, key string) (err error) {
	data := make(map[string]string)
	data["name"] = name
	data["key"] = key
	err = write_msg(writer, "join", data)
	return
}

func send_ping(writer *bufio.Writer) (err error) {
	err = write_msg(writer, "ping", make(map[string]string))
	return
}

func send_throttle(writer *bufio.Writer, throttle float32) (err error) {
	err = write_msg(writer, "throttle", throttle)
	return
}

func dispatch_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	switch msgtype {
	case "join":
		log.Printf("Joined")
		send_ping(writer)

	case "gameInit": // Let's find out the stuff needed for max speed: smallest radius, and friction.

		log.Printf("GAME INIT MESSAGE GOT NAU YES ")

		// TODO: how did i get stuff out of the json message

		log.Printf("data has this inside it: ")
		log.Printf("%v", data)

		/* Tell me this can't be right. Maps inside maps inside maps inside maps inside maps inside maps sdfgdsdf.
			Better yet: just conjure me usable data structures out of this 'data' map monster.

		var race, track, pieces map[string]interface{}

		var ok bool

		// msg, ok = input.(map[string]interface{})
		race, ok = data.(map[string]interface{})

		if !ok {
			send_ping(writer)
		}

		log.Printf("Trying to get race out of map")
		log.Printf("%v", race["race"])

		// still peeling through
		track, ok = race["race"].(map[string]interface{})

		log.Printf("Trying to get race out of map")
		log.Printf("%v", track["track"])

		// adn still....
		pieces, ok = track["pieces"].(map[string])

		// how many times do i have to loop?


		// 1) get me a list of track pieces, so that i can loop over all pieces and get info on them
		*/
		friction = 0.5 // default guess?
		radius = 50

	case "gameStart":
		log.Printf("Game started")
		send_ping(writer)
	case "crash":
		log.Printf("Someone crashed")

		// Obviously we had mis-estimated the friction?
		friction *= 0.9

		send_ping(writer)
	case "gameEnd":
		log.Printf("Game ended")
		// Exit is not strictly necessary?
		os.Exit(0)
	case "carPositions":

		var kaasu float32 = getMaxThrottle(radius, friction)

		// We need to get some info on how our car is moving:
		// TODO: get speed.

		send_throttle(writer, kaasu)

		// log.Printf("Set throttle to %f. Max speed: %f", kaasu, getMaxSpeed(radius, friction))

	case "error":
		log.Printf(fmt.Sprintf("Got error: %v", data))
		send_ping(writer)
	default:
		log.Printf("Got msg type: %s", msgtype)
		send_ping(writer)
	}
	return
}

func parse_and_dispatch_input(writer *bufio.Writer, input interface{}) (err error) {
	switch t := input.(type) {
	default:
		err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
		return
	case map[string]interface{}:
		var msg map[string]interface{}
		var ok bool

		msg, ok = input.(map[string]interface{})
		if !ok {
			err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
			return
		}

		switch msg["data"].(type) {
		default:
			err = dispatch_msg(writer, msg["msgType"].(string), nil)
			if err != nil {
				return
			}
		case interface{}:
			err = dispatch_msg(writer, msg["msgType"].(string), msg["data"].(interface{}))
			if err != nil {
				return
			}
		}
	}
	return
}

func bot_loop(conn net.Conn, name string, key string) (err error) {
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	send_join(writer, name, key)
	for {
		input, err := read_msg(reader)
		if err != nil {
			log_and_exit(err)
		}
		err = parse_and_dispatch_input(writer, input)
		if err != nil {
			log_and_exit(err)
		}
	}
}

func parse_args() (host string, port int, name string, key string, err error) {
	args := os.Args[1:]
	if len(args) != 4 {
		return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]

	return
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func main() {

	host, port, name, key, err := parse_args()

	if err != nil {
		log_and_exit(err)
	}

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)

	conn, err := connect(host, port)

	if err != nil {
		log_and_exit(err)
	}

	defer conn.Close()

	err = bot_loop(conn, name, key)
}
