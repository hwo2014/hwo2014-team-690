# -*- coding: utf-8 -*-

import json
import socket
import sys
import math
import datetime


track = None
global_throttle = 0
global_tick     = 0
just_started    = True
g_friction = 1.5
g_rolling_friction = 0
g_obstacle_near = False
g_just_switched = [False, global_tick]
g_max_speed = 15
g_max_angle_growth = 2.0
g_max_angle = 58

OBSTACLE_DISTANCE = 50
GLOBAL_FRICTION_TUNER = 0.95
GLOBAL_ANGLE_GROWTH_TUNER = 0.8




# 
class Car_State(object):
    def __init__(self, angle, speed, position, throttle = 0):
        global global_throttle

        self.speed = speed
        self.angle = angle
        self.position = position
        self.throttle = global_throttle

class Car(object):
    def __init__(self, data): 
        self.name = data['id']['name']
        self.color = data['id']['color']

        self.state = Car_State(0, 0, Position({"pieceIndex": 0, "inPieceDistance": 0, "lane": {"startLaneIndex": 0, "endLaneIndex": 0}, "lap": 0}), 0)

        self.history = []

        for i in xrange(10):
            self.history.append(self.state)

    # needs helpers / functions. 

    def update_history(self):
        self.history.insert(0, self.state)
        if len(self.history) > 10:
            self.history.pop()

    def calculate_speed(self, position):
        global global_tick
        global  just_started
        if global_tick == 0 or just_started:
            just_started = False
            return 0

        current_distance = position.in_piece_distance
        last_distance    = self.history[0].position.in_piece_distance

        prev_piece = self.history[0].position.piece_index

        # In case of different pieces
        if position.piece_index != prev_piece:
            print "adding last piece length: +", track.pieces[prev_piece].get_length(position.start_lane) 
            current_distance += track.pieces[prev_piece].get_length(position.start_lane)

        speed = current_distance - last_distance

        return speed

    def calculate_acceleration(self):
        return self.history[0].speed - self.history[1].speed

    def centripetal_acceleration(self):
        # a = v^2 / r
        if track.pieces[self.state.position.piece_index].radius:
            return (self.state.speed ** 2) / track.pieces[self.state.position.piece_index].radius
        else:
            return 0

        # note: i hear this value plays a major role in slip angle dynamics. 

    def update_state(self, car_data):

        position = Position(car_data['piecePosition'])
        speed = self.calculate_speed(position)
        self.state = Car_State(car_data['angle'], speed, position)
        self.update_history()

    def __str__(self):
        return "Speed: %.4f\tAngle: %.4f\tC-Accel: %.4f\tThrottle: %.6f" % (self.state.speed, self.state.angle, self.centripetal_acceleration(), self.state.throttle)



# questionable 




# OK / complete classes : 


class Track(object):
    def __init__(self, data):
        self.pieces = []
        self.lanes = []

        print "Track data here: ..."
        print data

        for piece in data['pieces']:
            self.pieces.append(Piece(piece))

        for lane in data['lanes']:
            self.lanes.append(Lane(lane))

        print "Printing out pieces: "

        for piece in self.pieces:
            print piece





class Position(object):
    def __init__(self, data):
        self.piece_index = data['pieceIndex']
        self.in_piece_distance = data['inPieceDistance']
        self.start_lane = data['lane']['startLaneIndex']
        self.end_lane = data['lane']['endLaneIndex']
        self.lap = data['lap']

        self.crashed = False

class Lane(object):
    def __init__(self, data):
        self.index = data['index']
        self.distance_from_center = data['distanceFromCenter']

class Piece(object):
    def __init__(self, data):

        try:
            self.length = data['length']
            self.angle = False
            self.radius = False
        except KeyError:
            self.length = False
            self.angle = data['angle']
            self.radius = data['radius']

        try:
            self.switch = data['switch']
        except KeyError:
            self.switch = False

        if self.length:
            self.max_speed_factor = False
        else:
            self.max_speed_factor = math.sqrt(self.radius * g_friction)
            # sqrt(g_friction * gravity * radius) 


    def get_length(self, lane = None):
        # Doesn't account for length differences caused by switching on switch pieces. 

        if self.length:
            return self.length
        else: 
            radius = self.radius
            global track

                     

            if self.angle < 0:
                radius += track.lanes[lane].distance_from_center
            else:
                radius -= track.lanes[lane].distance_from_center


            return 2 * math.pi * radius * ( abs(self.angle) / 360.0)

    def __str__(self):
        return "Length: "+ str(self.length) +", Angle: "+ str(self.angle) +", Radius: " + str(self.radius) + ", Switch: " + str(self.switch)

class Turbo(object):
    def __init__(self, duration_ms, duration_ticks, factor, available):
        self.duration_ms = duration_ms
        self.duration_ticks = duration_ticks
        self.factor = factor
        self.available = available









# Le bot: 



class BpsPythonbot(object):

    def __init__(self, socket, name, key, debug1=None, debug2=None, debug3=None, debug4=None):
        self.car = None
        self.race = None
        self.socket = socket
        self.name = name
        self.key = key

        self.other_cars = []

        self.want_to_switch = False

        self.max_speed = 7
        self.max_angle = 60
        self.min_radius = 100.0

        self.last_track_piece = 0

        self.track = 0
        self.debug1 = debug1
        self.debug2 = debug2
        self.debug3 = debug3
        self.debug4 = debug4

        self.turbo = Turbo(0, 0, 1, False)

    def get_own_car(self, data):
        for car in data:
            if car['id']['name'] == self.name:
                return car

        # we're not supposed to get here
        raise Exception

    def got_turbo(self, data):
        self.turbo = Turbo(data['turboDurationMilliseconds'], data['turboDurationTicks'], data['turboFactor'], True)


    def own_inpiece_distance(self, data):
        # print "In piece distance:", data[0]['piecePosition']['inPieceDistance']
        return data[0]['piecePosition']['inPieceDistance']

    def piece_length(self, data, piece_index):
        return 10.0

    def own_piece_index(self, data):
        return data[0]['piecePosition']['pieceIndex']

    def msg(self, msg_type, data, do_we_tick=False):
        global global_tick
        if do_we_tick:
            print("[%s] %s: %s @Tick %d" % (datetime.datetime.now().time(), msg_type, data, global_tick))
            self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": global_tick}))
        else:
            self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join_testrace(self, trackname):
        return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": trackname })

    def join(self):
        return self.msg("join", {"name": self.name, "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle, True)
        global global_throttle
        global_throttle = throttle

    def ping(self):
        self.msg("ping", {})

    def run(self):
        if not self.debug1 or not self.debug2 or not self.debug3 or not self.debug4:
            self.join()
        else:
            if debug1 == "host":
                self.msg("createRace", {"botId": {"name": self.name, "key": self.key}, "trackName": debug2, "password": debug4, "carCount": int(debug3) })
            else:
                self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": debug2, "password": debug4, "carCount": int(debug3) })

        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def switch(self, index):
        print("switching lane towards: ", index)
        direction = "Triple compile!"

        if index > self.car.state.position.start_lane:
            direction = "Right"
        else:
            direction = "Left"

        self.msg("switchLane", direction, True)

        print "Switched lane towards: ", direction



    def own_angle(self, data):
        self.angle = data[0]['angle']
        return self.angle

    def ebin_turbo_chance(self, data):
        i = self.own_piece_index(data)
        straight_length = 0

        while (self.track.pieces[(i) % len(self.track.pieces) ].length):
            straight_length += self.track.pieces[(i) %  len(self.track.pieces)].length
            i += 1

        if straight_length > 600:
            return True
        else: 
            return False

    def detect_obstacle(self):
        global g_obstacle_near
        g_obstacle_near = False

        own_position = self.car.state.position.in_piece_distance
        own_piece = self.car.state.position.piece_index
        own_lane = self.car.state.position.start_lane

        closest_distance = 9999999.0

        for car in self.cars: 
            if car.state.position.piece_index == own_piece and car.state.position.start_lane == own_lane:
                distance = car.state.position.in_piece_distance - own_position
                if distance < OBSTACLE_DISTANCE and distance < closest_distance:
                    closest_distance = distance
                    g_obstacle_near = True 

        return g_obstacle_near  


    def update_cars(self, data):
        for car_data in data:
            if car_data['id']['name'] == self.name:
                self.car.update_state(car_data)
            else:
                for car in self.cars:
                    if car_data['id']['name'] == car.name:
                        car.update_state(car_data)
                        continue

    def get_max_speed(self, index):
        return g_max_speed * track.pieces[index].max_speed_factor

    def get_next_turn(self): 
        distance = 0
        max_speed = 0
        i = 1
        current_piece = self.car.state.position.piece_index


        if track.pieces[current_piece].max_speed_factor:
            max_speed = self.get_max_speed(current_piece)
            distance = 0
        else:
            distance += track.pieces[current_piece].length() - self.car.state.position.in_piece_distance

            while not track.pieces[(current_piece + i) % len(track.pieces)].max_speed_factor:
                distance += track.pieces[(current_piece + i) % len(track.pieces)].length()
                i += 1

        return distance, max_speed







    def on_car_positions(self, data):
        global global_throttle
        global global_tick
        global g_obstacle_near
        global g_just_switched
        global track
        global g_max_speed

        # Update the state of the car
        self.update_cars(data)

        # Local variables
        speed = self.car.state.speed
        angle = self.car.state.angle
        piece_index = self.car.state.position.piece_index

        print "Detecting obstacles..."
        g_obstacle_near = self.detect_obstacle()

        print "Finding max speed for current turn..."
        if track.pieces[piece_index].max_speed_factor:
            max_speed = self.get_max_speed(piece_index)
        else:
            i = piece_index
            while not track.pieces[i].max_speed_factor:
                i = (i + 1) % len(track.pieces)
            max_speed = self.get_max_speed(i)


        print("[%s] Tick %d, Car %s, %s" % (datetime.datetime.now().time(), global_tick, self.get_own_car(data)['id']['name'], self.car))

        # Sanity check global_throttle
        if global_throttle != 0 and global_throttle < 0.1:
            global_throttle = 0.5

        # 1st Priority: Turbo
        print "See if we have a turbo chance."
        if (self.turbo.available and self.ebin_turbo_chance(data)):
            self.msg('turbo', "DOUBLE COMPILE!", True)
            self.turbo.available = False
            print "DOUBLE COMPILE!"
            return

        # 2nd Priority: Switching lanes if obstacles are detected
        if (g_obstacle_near):
            print "Obstacle detected!"
            self.want_to_switch = True

            if abs(global_tick - g_just_switched[1]) > 30:
                print "It has been long enough since last switch!"
                g_just_switched[0] = False

            if track.pieces[(piece_index + 1) % len(track.pieces)].switch and not g_just_switched[0]:
                print "Next piece is a switch piece! Sending switch message."
                # choose which track to switch to

                own_lane = self.car.state.position.start_lane
                available_lanes = []
                for lane in track.lanes:
                    if (lane.index + 1 == own_lane or lane.index - 1 == own_lane):
                        available_lanes.append(lane.index)

                if len(available_lanes) > 1:
                    # TODO: choose least-congested lane
                    free_lane = -1 
                    min_cars = 99
                    for lane in available_lanes: 
                        cars_on_lane = 0
                        for car in self.cars: 
                            if car.state.position.start_lane == lane:
                                cars_on_lane += 1

                        if cars_on_lane < min_cars:
                            min_cars = cars_on_lane
                            free_lane = lane
                    self.switch(free_lane)

                else:
                    self.switch(available_lanes[0])

                g_just_switched = [True, global_tick]

                return



        # 3rd Priority: Watch out for too fast angle growth
        print "Checking if angle is growing too fast..."
        if abs(abs(angle) - abs(self.car.history[1].angle)) > g_max_angle_growth:
            print "Angle is growing too fast! ",
            if abs(angle) > 20:
                print "Really too fast, braking!"
                self.throttle(global_throttle * 0.2)
                
                return
            else: 
                print "Quite fast, slowing down!"
                self.throttle(global_throttle * 0.3)
                return
        elif abs(angle) > 40: 
            print "Angle not _growing too fast but still too big. braking."
            self.throttle(0.0)
            return


        # 4th Priority: Slow down if going too fast in current turn
        print "Cheking if we are going too fast in current turn..."
        

        if track.pieces[piece_index].max_speed_factor:

            if speed < self.get_max_speed(piece_index):
                print "Not enough speed, full throttle!"
                self.throttle(1.0)
                return

            else:
                if speed < self.max_speed * 1.3:
                    print "Wow, slow down quite a bit."
                    global_throttle *= 0.45
                else:
                    print "Slow down a bit."
                    global_throttle *= 0.7
                self.throttle(global_throttle)
                return



        # 5th Priority: Slow down if we are going too fast at a tight turn. 

        print "Checking if next piece has speed limits"
        if speed > max_speed and self.track.pieces[(self.own_piece_index(data) + 2)  % len(self.track.pieces)].max_speed_factor:
            print "Piece 2 pieces ahead has a limit, and we're going too fast. slowing down."
            global_throttle *= 0.8
            self.throttle(global_throttle)

        elif self.track.pieces[(self.own_piece_index(data) + 1) % len(self.track.pieces)].max_speed_factor:
            print "Next piece has a limit, and we're going too fast. slowing down."
            if speed > self.get_max_speed(self.car.state.position.piece_index):
                global_throttle *= 0.7
                self.throttle(global_throttle)

            else:
                print "Not enough speed, full throttle!"
                self.throttle(1.0)
        else:
            print "No problems detected, full throttle!"
            self.throttle(1.0)



    def on_crash(self, data):
        print("Someone crashed")
        global g_friction
        global g_max_speed
        global g_max_angle_growth
        global g_max_angle

        crash_car = None

        for car in self.cars:
            if data['name'] == car.name:
                crash_car = car

        if not crash_car:
            if data['name'] == self.name: 
                crash_car = self.car

        crash_last_speed = crash_car.history[1].speed
        crash_last_angle = crash_car.history[1].speed
        crash_last_angle_growth = abs( abs(crash_car.history[1].angle) - abs(crash_car.history[2].angle))
        crash_piece_index = crash_car.history[1].position.piece_index
        crash_piece_radius = track.pieces[crash_piece_index].radius

        if crash_piece_radius != 0:
            old_v_max = g_max_speed * track.pieces[crash_piece_index].max_speed_factor
            friction_max = (old_v_max**2) / (g_max_speed**2 * crash_piece_radius) 

            if friction_max < g_friction:
                g_friction = friction_max * GLOBAL_FRICTION_TUNER

        print "global friciton to:", g_friction


        if crash_last_angle_growth < g_max_angle_growth:
            g_max_angle_growth = crash_last_angle_growth * GLOBAL_ANGLE_GROWTH_TUNER

        print "set global max angle growth to:", g_max_angle_growth

        if crash_last_angle < g_max_angle:
            # g_max_angle = 0.99 * crash_last_angle
            pass

        print "set global max angle to:", g_max_angle

        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        global just_started
        global track

        # parse tightest curve.
        print "Game init data: "

        print data
        self.track = Track(data['race']['track'])
        self.car   = Car(self.get_own_car(data['race']['cars']))
        self.cars  = []

        for car in data['race']['cars']:
            if car['id']['name'] != self.name:
                self.cars.append(Car(car))




        track = self.track
        just_started = True


        self.ping()


    def msg_loop(self):
        global global_tick

        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'turboAvailable': self.got_turbo
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            try:
                global_tick = msg['gameTick']
            except KeyError:
                pass

            print("[%s] Messagetype %s received" % (datetime.datetime.now().time(), msg_type))

            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    debug1 = None
    debug2 = None
    debug3 = None
    debug4 = None
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        if len(sys.argv) == 9:
            debug1 = sys.argv[5]
            debug2 = sys.argv[6]
            debug3 = sys.argv[7]
            debug4 = sys.argv[8]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = BpsPythonbot(s, name, key, debug1, debug2, debug3, debug4)
        bot.run()




